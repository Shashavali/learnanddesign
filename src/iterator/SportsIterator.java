package iterator;

import java.util.ArrayList;
import java.util.List;

import assignmentIteratorPattern.ChannelsIterator;

public class SportsIterator implements ChannelsIterator {
	
	int index;
    List<String> sportsChannel = new ArrayList<String>();
    
    public SportsIterator(List<String> sportsChannel) {
        this.sportsChannel = sportsChannel;
    } 

	@Override
    public void first() {
        index = 0;
    }
 
    @Override
    public boolean hasNext() {
        if(index < sportsChannel.size())
            return true;
        else
            return false;
    }
 
    @Override
    public String next() {
        return sportsChannel.get(index++);
    }

}
