package assignmentIteratorPattern;

public interface ChannelsIterator {
	
	void first();
    boolean hasNext();
    String next();

}
