package assignmentIteratorPattern;

import java.util.List;

public interface Channels {
	
	ChannelsIterator createIterator();
}
