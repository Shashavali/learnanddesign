package driver;



import assignmentIteratorPattern.ChannelsIterator;
import plans.BasicPlan;
import plans.StandardPlan;

public class driverTest {

	public static void main(String[] args) {

		BasicPlan bp = new BasicPlan();
		ChannelsIterator iterator = bp.createIterator();
		System.out.println("List of sports channels for Basic Plan");
		System.out.println("--------------------------------------");
		while(iterator.hasNext()){
			System.out.println(iterator.next());
		}
		
		StandardPlan standardPlan = new StandardPlan();
		ChannelsIterator iterator1 = standardPlan.createIterator();
		System.out.println("List of sports channels for Standard Plan");
		System.out.println("-----------------------------------------");
		while(iterator1.hasNext()){
			System.out.println(iterator1.next());
		}
		
	}

}
