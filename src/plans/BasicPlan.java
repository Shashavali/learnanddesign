package plans;

import java.util.ArrayList;
import java.util.List;

import assignmentIteratorPattern.Channels;
import assignmentIteratorPattern.ChannelsIterator;
import iterator.SportsIterator;

public class BasicPlan implements Channels {

	List<String> sports = new ArrayList<String>();
	List<String> movies = new ArrayList<String>();
	List<String> news = new ArrayList<String>();
	List<String> educational = new ArrayList<String>();
	List<String> entertainment = new ArrayList<String>();
	List<String> categoryType;

	public BasicPlan() {
		sports.add("DD Sports");
		sports.add("DD Sports1");
		movies.add("Zee Cinema");
		movies.add("DD Cinema");
	}
	

	@Override
	public ChannelsIterator createIterator() {
		return new SportsIterator(sports);
	}

}
