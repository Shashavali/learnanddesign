package plans;

import java.util.ArrayList;
import assignmentIteratorPattern.Channels;
import java.util.List;

import assignmentIteratorPattern.ChannelsIterator;
import iterator.SportsIterator;

public class StandardPlan implements Channels {
	
	List<String> sports = new ArrayList<String>();
	List<String> movies = new ArrayList<String>();
	List<String> news = new ArrayList<String>();
	List<String> educational = new ArrayList<String>();
	List<String> entertainment = new ArrayList<String>();
	
	public StandardPlan(){
		sports.add("Star Sports");
		sports.add("Star Sports1");
	}

	@Override
	public ChannelsIterator createIterator() {
		return new SportsIterator(sports);
	}
}
